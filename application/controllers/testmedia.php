<?php
class Testmedia extends CI_Controller{
	function __construct(){
		parent::__construct();
	}

	function index(){
		$Judul="Test Media";
		$data['judul']=$Judul; //$judul disini harus sama dengan $judul di baris ke 8
		$this->template->load('media','testmedia',$data);
		 // template->load : menampilkan data dengan template  
		// media : nama file dari tamplate yang kita buat
		// test media : nama file yang ingin kita tampilkan 
	}
}