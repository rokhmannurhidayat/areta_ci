<?php
class Kelas Extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_kelas');
	}

	function input()
	{
		$judul="Input Data Kelas";
		$data['judul']="$judul";
		//$this->load->view('input_kelas',$data,FALSE);
		$this->template->load('media', 'input_kelas',$data);
	}

	function tampil()
	{
		$judul="Tampil Data Kelas";
		$data['judul']="$judul";
		$data['tampil']=$this->M_kelas->tampil()->result();
		//$this->load->view('tampil_kelas',$data,FALSE);
		$this->template->load('media', 'tampil_kelas',$data);
	}

	function hapus()
	{
		$id=$this->uri->segment(3);
		$this->db->where('kd_kelas',$id);
		$this->db->delete('kelas');
		redirect('kelas/tampil');
	}

	function edit()
	{
		$id=$this->uri->segment(3);
		$judul="Edit Data Kelas";
		$data['judul']="$judul";
		$data['edit']=$this->M_kelas->getId($id)->row_array();
		//$this->load->view('edit_kelas',$data,FALSE);
		$this->template->load('media', 'edit_kelas',$data);
	}

	function simpan()
	{
		$data=array(
			'kd_kelas'=>$this->input->post('kode_kelas'),
			'nama'=>$this->input->post('nama'),
			'jumlah'=>$this->input->post('jumlah'),
		);
		$this->M_kelas->simpan($data);
		redirect('kelas/tampil','refresh');
	}

	function update()
	{
		$id=$this->input->post('kode_kelas');
		$data=array(
			'kd_kelas'=>$this->input->post('kode_kelas'),
			'nama'=>$this->input->post('nama'),
			'jumlah'=>$this->input->post('jumlah'),
		);
		$this->M_kelas->update($data,$id);
		redirect('kelas/tampil','refresh');
	}
}

