<?php
class guru extends CI_Controller {
	function __construct(){
	parent::__construct();
	$this->load->model('M_guru');
	}

	function index()
	{
			
	}

	function input()
	{
		$judul="Input Data guru";
		$data['judul']="$judul";
		//$this->load->view('input_guru',$data,FALSE);
		$this->template->load('media', 'input_guru',$data);
	}

	function tampil()
	{
		$judul="Tampil Data guru";
		$data['judul']="$judul";
		$data['tampil']=$this->M_guru->tampil()->result();
		//$this->load->view('tampil_guru',$data,FALSE);
		$this->template->load('media', 'tampil_guru',$data);
	}

	function hapus()
	{
		$id=$this->uri->segment(3);
		$this->db->where('nik',$id);
		$this->db->delete('guru');
		redirect('guru/tampil');
	}

	function edit()
	{
		$id=$this->uri->segment(3);
		$judul="Edit Data guru";
		$data['judul']="$judul";
		$data['edit']=$this->M_guru->getId($id)->row_array();
		//$this->load->view('edit_guru',$data,FALSE);
		$this->template->load('media', 'edit_guru',$data);
	}

	function simpan()
	{
		$data=array( 
			'nik'=>$this->input->post('nik'), 
			'nama'=>$this->input->post('nama'), 
			'email'=>$this->input->post('email'),
			'alamat'=>$this->input->post('alamat'),
			'hp'=>$this->input->post('HP'),
			'status'=>$this->input->post('status'),
			'jenis_kelamin'=>$this->input->post('Jenis_Kelamin'),
			'pasword'=>$this->input->post('Password'),
			'tanggal_lahir'=>$this->input->post('Tanggal_Lahir'),
			);
		$this->M_guru->simpan($data); //mberfungsi untuk mengirim data ke model
		redirect('guru/tampil','refresh'); //berfungsi untuk memanggil tampil siswa
	}
		
	function update()
	{
		$id=$this->input->post('nik');
		$data=array(
			'nik'=>$this->input->post('nik'),
			'nama'=>$this->input->post('nama'),
			'email'=>$this->input->post('email'),
			'alamat'=>$this->input->post('alamat'),
			'hp'=>$this->input->post('HP'),
			'status'=>$this->input->post('status'),
			'jenis_kelamin'=>$this->input->post('Jenis_Kelamin'),
			'pasword'=>$this->input->post('Password'),
			'tanggal_lahir'=>$this->input->post('Tanggal_Lahir'),
			);
		$this->M_guru->update($data,$id);
		redirect('guru/tampil','refresh');
	}
}