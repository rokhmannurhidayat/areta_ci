<?php
class login extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_login');
	}

	function index()
	{
		$judul="Halaman Login";
		$data['judul']=$judul;
		$this->load->view('login',$data);
	}

	function auth()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$cek = $this->M_login->login($username,$password);
		if ($cek == 1) 
		{
			redirect('dashboard');
		}
		else
		{
			redirect('login');
		}
	}

	function logout()
	{
		session_destroy();
		redirect('login');
	}
}