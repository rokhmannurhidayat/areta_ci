<?php
class Jadwal extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model(array('M_jadwal','M_guru','M_kelas','M_mapel','M_ruang','M_siswa'));
	}

	function index() { //fun index digunakan untuk menampilkan data
		$judul="Data Jadwal";
		$data['judul']=$judul;
		$data['tampil']=$this->M_jadwal->tampil()->result();
		$this->template->load('media', 'jadwal/tampil',$data);	
	}

	function input() {
		$judul="Input jadwal";
		$data['judul']="$judul";
		// ini adalah query untuk menampilkan data dari table 
		$data['nik']=$this->M_guru->tampil()->result();
		$data['kelas']=$this->M_kelas->tampil()->result();
		$data['mapel']=$this->M_mapel->tampil()->result();
		$data['ruang']=$this->M_ruang->tampil()->result();
		$this->template->load('media', 'jadwal/input',$data);
	}	

	function tampil()
	{
		$judul="Tampil jadwal";		//ini adalah judul
		$data['judul']="$judul";	//variabel judul
		$data['tampil']=$this->M_jadwal->tampil()->result(); //lempar data ke model/menggambil data dari model 
		//$this->load->view('tampil_siswa',$data,FALSE);	//ini untuk menampilkan ke view
		$this->template->load('media', 'jadwal/tampil',$data);
	}

function edit()
	{
		$id=$this->uri->segment(3);
		$judul="Edit Data Jadwal";
		$data['judul']="$judul";
		$data['edit']=$this->M_jadwal->getId($id)->row_array();
		//$this->load->view('edit_guru',$data,FALSE);
		$this->template->load('media', 'jadwal/edit',$data);
	}

	function simpan()
	{
		$data=array( 
			'nik'=>$this->input->post('nik'), //nim sebelah kanan harus sama dengan nim yang di input.php
			'jam'=>$this->input->post('jam'), //nim sebelah kiri harus sama dengan nim yang di database
			'hari'=>$this->input->post('hari'),
			'kd_kelas'=>$this->input->post('kelas'),
			'kd_mapel'=>$this->input->post('mapel'),
			'kd_ruang'=>$this->input->post('ruang'),
			);
		$this->M_jadwal->simpan($data); //mberfungsi untuk mengirim data ke model
		redirect('jadwal/tampil','refresh'); //berfungsi untuk memanggil tampil siswa
	}
 
		
}